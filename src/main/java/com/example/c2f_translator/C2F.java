package com.example.c2f_translator;

import java.io.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;

@WebServlet(name = "C2F", value = "/c2f")
public class C2F extends HttpServlet {
    public void init() {}

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.setContentType("text/html");

        String title = "C2F Translator";
        String begHtml ="<!DOCTYPE html>" +
                        "<html>" +
                            "<head>" +
                                "<title>" + title + "</title>" +
                            "</head>" +
                            "<body>" +
                                "<h1>" + title + "</h1>";
        String endHtml =    "</body>" +
                        "</html>";

        boolean check;
        try {
            double i = Double.parseDouble(request.getParameter("celsius"));
            check = true;
        } catch (NumberFormatException e) {
            check = false;
        }

        if (!request.getParameter("celsius").isEmpty() && check) {
            response.getWriter().println(
                        begHtml +
                            "<p>" +
                                "Temperature of [" + request.getParameter("celsius") + "] Celsius to Fahrenheit is [" + ComputingC2F.toFahrenheit(Double.parseDouble(request.getParameter("celsius"))) + "]" +
                            "</p>" +
                        endHtml);
        } else {
            response.getWriter().println(
                        begHtml +
                            "<p>An Error has occurred with the input</p>" +
                        endHtml);
        }
    }

    public void destroy() {}
}
package com.example.c2f_translator;

public class ComputingC2F {
    public static double toFahrenheit(double celsius) {
        return (celsius * 1.8) + 32;
    }
}
